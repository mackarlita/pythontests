from flask import Flask

application = Flask(__name__)

application.add_url_rule('/', 'root', lambda: 'ok')
application.add_url_rule('/hello', 'hello_world', lambda: 'Hello, World!')

if __name__ == '__main__':
	application.run()
