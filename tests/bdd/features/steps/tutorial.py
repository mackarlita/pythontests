from behave import *

@given('We have behave installed')
def step_impl(context):
    pass

@when('We implement a test')
def step_impl(context):
    assert True is not False

@then('Behave will test it for us!')
def step_impl(context):
    assert context.failed is False